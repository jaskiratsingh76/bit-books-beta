/***************************************************
 * bitbook_script.js
 * 
 * JavaScript for Bit Books
 * Nokia Asha 
 *
 * Series 40 
 *
 * Jaskirat Singh
 * 6 March 2013
 *
 ***************************************************/

 /* test javascript for testing ajax */
function geolocate() {
	
	$.ajax({
		url: 'http://freegeoip.net/json/',
		dataType: 'json',
		async: false,
		crossDomain: false,
		success: function(data, textStatus, jqXHR) {
			var html = '';
			$.each(data, function(key, value) {
				html += '<li><b>' + key + '</b>: ' + value + '</li>';
			});
			$('#list').html(html);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$('#list').html('<li><b>ERROR</b>: '+textStatus+'</li>');
		}
	});
};

/******************************************
 * BitBooks JS begins here
 ******************************************/

var BB_Global = {
	/**
	 * version 0 uses Flipkart
	 * version 1 uses Amazon
	 * 
	 * Above websites used to display
	 * listings to BUY.
	 */
	version: 0,
}; 

var Links = {
	rating_images : new Array(),

	init:function() {
		Links.rating_images[0] = 'assets/images/rating_2_0.gif';
		Links.rating_images[1] = 'assets/images/rating_2_5.gif';
		Links.rating_images[2] = 'assets/images/rating_3_0.gif';
		Links.rating_images[3] = 'assets/images/rating_3_5.gif';
		Links.rating_images[4] = 'assets/images/rating_4_0.gif';
		Links.rating_images[5] = 'assets/images/rating_4_5.gif';
		Links.rating_images[6] = 'assets/images/rating_5_0.gif';
	},

	getLinks: function(index) {
		return Links.rating_images[index];
	}
};

var Search = {
	url: 'https://www.googleapis.com/books/v1/volumes?maxResults=10&key=AIzaSyArhpkJPtKkn3o7OXq804bnZhsbEp_IMtE&q=',
	query: '',
	price_url_flipkart_title: 'http://m.flipkart.com/m/m-search-all/searchCategory?store=buk&count=10&q=',
	price_url_flipkart_isbn: 'http://www.flipkart.com/m/m-search-all/searchAll?otracker=search&submit=&q=',
	price_url_amazon: 'http://www.amazon.com/gp/aw/s/ref=aa_sbox_dept_filter?url=n%3D283155&k=',
	merchant_id_flipkart: 0,
	merchant_id_amazon: 1,

	init: function() {
		Links.init();
		Search.getQuery();
		mwl.hide('#intro');
		Search.getResult();
	},

	getQuery: function() {
		Search.query = document.getElementById('q').value;
	},

	formURL: function() {
		url = Search.url;
		Search.query = encodeURI(Search.query);
		url = url + Search.query;
		return url;
	},

	getResult: function() {
		$.ajax({
		url: Search.formURL(),//'https://www.googleapis.com/books/v1/volumes?maxResults=10&q=norwegian%20wood',
		dataType: 'json',
		async: false,
		crossDomain: false,
		success: Search.parseResult
		});
	},

	parseResult: function(result, textStatus, jqXHR) {		
		
		// display raw input for testing
		/*
		var html = '';
		$.each(result, function(key, value) {
			html += '<li><b>' + key + '</b>: ' + value + '</li>';
		});
		$('#list').html(html);
		*/

		// initialise
		search_html = "<table id='results_table'>";
		row_count = 0;
		flag_isbn = 0;
		desc_html = '';

		// extract data from json response
		for (i = 0; i < result.items.length; i++)
		{
			flag_isbn = 0;
			authors = '';
			category = '';
			
			try{
				title = result.items[i].volumeInfo.title;
			}
			catch(e) {
				title = '';
			}
			search_query_title = Search.stripChars(title);
			
			try{
				description = result.items[i].volumeInfo.description;
			}
			catch(e) {
				description = '';
			}
			
			try{
				pageCount = result.items[i].volumeInfo.pageCount;
			}
			catch(e) {
				pageCount = '';
			}
		
			try{
				year_published = result.items[i].volumeInfo.publishedDate.substr(0, 4);
			}
			catch(e) {
				year_published = '';
			}

			try{
				rating = result.items[i].volumeInfo.averageRating;
			}
			catch(e) {
				rating = 2.0;
			}
			rating_link = Search.getRatingImage(rating);

			// publisher
			try {
				publisher = result.items[i].volumeInfo.publisher;
			}
			catch(e) {
				publisher = '';
			}

			// ISBN
			try	{
				isbn13 = result.items[i].volumeInfo.industryIdentifiers[1].identifier;
				isbn13_formatted = Search.formatISBN(isbn13);
			}	
			catch(e) {
				flag_isbn = 1;
			}

			// image link
			try {
				image_link = result.items[i].volumeInfo.imageLinks.thumbnail;
				image_link = Search.removeCurl(image_link);
			}
			catch(e) {
				desc_image = '';
			}

			// author(s) name
			try {
				num_authors = result.items[i].volumeInfo.authors.length;
			}
			catch(e) {
				num_authors = 0;
			}

			for (j = 0; j < num_authors; j++)
			{
				if(j > 0)
					authors += ", ";
				authors += result.items[i].volumeInfo.authors[j];
			};
			
			// category of book
			try {
				for (j = 0; j < result.items[i].volumeInfo.categories.length; j++)
				{
					if(j > 0)
						category += ", ";
					category += result.items[i].volumeInfo.categories[j];
				};
			}
			catch(e) {
				category = '';
			}
			

			// housekeeping
			if((!publisher) || (publisher == undefined))
				publisher = '';
			if((!pageCount) || (pageCount == undefined))
				pageCount = '\'x\'';
			if((!category) || (category == undefined))
				category = '';
			if(flag_isbn == 1)
				isbn13 = 'ISBN not available :(';
			if((!description) || (description == undefined))
				description = 'Sorry, no description available :(';

			/**********************************************
		     * GENERATE SEARCH RESULTS PAGE
             **********************************************/
            search_html += '<tbody onclick="Navigation.showDesc(' + i + ');">';
            search_html += "<tr><td class='bb_item_first' rowspan='5'><img class ='bb_search_item_image' src='" + image_link + "'></td>";
		    search_html += "<td class='bb_item_title bb_item_first'>" + title + "</td></tr>";
		    search_html += "<tr><td>" + authors + "</td></tr>";
		    search_html += "<tr><td>" + category + "</td></tr>";
		    search_html += "<tr><td>" + year_published + "</td></tr>";
		    search_html += "<tr><td>" + publisher + "</td></tr>";
		    search_html += "<tr><td class='bb_separator' colspan='2'></td></tr>";
		  	search_html += "</tbody>";

			/*************************************************
			 * GENERATING DESCRIPTION PAGES
			 *************************************************/
			desc_html += "<!-- page number 0 -->";
			desc_html += "<div id='desc_page_" + i + "'>";
			desc_html += "<!-- header -->";
			desc_html += "<div id='top' class='ui-header-desc'>";
			desc_html += '<div class="refresh-icon inline"><a onclick="Navigation.goBack(' + i + ');";><img alt="back" src="assets/images/back.png"></a></div>';
			desc_html += "</div>";
			
			desc_html += "<!-- page structure-->";
			desc_html += "<table class='bb_desc_table'>";
			desc_html += "<tbody>";
			desc_html += "<tr><td class='bb_desc_title' colspan='2'>" + title + "</td></tr>";
			desc_html += "<tr><td class='bb_desc_author'colspan='2'>By " + authors + "</td></tr>";
			desc_html += "<tr>";
			desc_html += "<td rowspan='6'><img class='bb_desc_image' src='" + image_link + "'/></td>";
			desc_html += "<td class='bb_desc_rating'><img src='" + rating_link + "'/></td>";
			desc_html += "</tr>";
			desc_html += "<tr><td class='bb_desc_pages'>" + year_published + " | " + pageCount + " pages</td></tr>";
			desc_html += "<tr><td class='bb_desc_pub'>" + publisher + "</td></tr>";
			desc_html += "<tr><td class='bb_isbn_label'>ISBN 13:</td></tr>";
			desc_html += "<tr><td class='bb_isbn_isbn'>" + isbn13_formatted + "</td></tr>";
			
			if(BB_Global.version == 0)
				desc_html += '<tr><td class="bb_desc_button"><div><button class="bb_button" onclick="Search.loadBuyResults(\'' + search_query_title + '\', 0, ' + Search.merchant_id_flipkart + ');">Buy on Flipkart</button></div></td></tr>';
			else if(BB_Global.version == 1)
				desc_html += '<tr><td class="bb_desc_button"><div><button class="bb_button" onclick="Search.loadBuyResults(\'' + search_query_title + '\', 0, ' + Search.merchant_id_amazon + ');">Buy on Amazon</button></div></td></tr>';

			desc_html += "<tr><td class='bb_desc_label' colspan='2'>Description</td></tr>";
			desc_html += "<tr><td class='bb_desc_desc' colspan='2'>" + description + "</td></tr>";
			desc_html += "</tbody>";
			desc_html += "</table></div>";
		};
		// END OF LOOP

		// close tags
		search_html += '</table>';

		// append search results
		$('#search_results').html(search_html);

		// append description pages
		$('#descriptions').html(desc_html);
	},

	removeCurl: function(link) {
		if(link.search('&edge=curl') != -1)
			link = link.replace('&edge=curl', '');
		return link;
	},

	stripChars: function(title) {
		// remove quotes, slashes from title string
		// to be appended to merchant's (amazon/flipkart) url as query
		title = title.replace(/'/g,"");
		title = title.replace(/"/g,"");
		title = title.replace(/\\/g,"");
		title = title.replace(/\//g,"");
		title = title.replace(/=/g,"");
		title = title.replace(/&/g,"");
		title = title.replace(/%/g,"");
		return title;
	},

	formatISBN: function(num) {
		isbn = num.substr(0, 3) + '-' + num.substr(3, 1) + '-' + num.substr(4, 3) + '-' + num.substr(7, 5) + '-' + num.substr(12, 1);
		return isbn;
	},

	getRatingImage: function(rating) {
		index = 0;
		switch(rating)
		{
			case 2.0:
				index = 0; break;

			case 2.5:
				index = 1; break;

			case 3.0:
				index = 2; break;

			case 3.5:
				index = 3; break;

			case 4.0:
				index = 4; break;

			case 4.5:
				index = 5; break;

			case 5.0:
				index = 6; break;
		}
		return Links.rating_images[index];
	},

	loadBuyResults: function(title, isbn, merchant_id) {
		// construct query string
		query = encodeURI(title);

		// check which merchant to load
		if(merchant_id == Search.merchant_id_flipkart)
		{
			// load results from Flipkart
			url = Search.price_url_flipkart_title + query;
		}
		else
		{
			// load results from Amazon
			url = Search.price_url_amazon + query;
		}
		mwl.loadURL(url);
	}
};


// more readable navigation
var Navigation = {
	goBack: function(id) {
		// construct page id string
		page_id = '#desc_page_' + id;
		
		//fire away
		mwl.hide(page_id);
		mwl.show('#search_page');
	}, 

	showDesc: function(id) {
		// construct page id string
		page_id = '#desc_page_' + id;

		// fire away
		mwl.hide('#search_page');
		mwl.show(page_id);
		mwl.scrollTo('#top');
	}
};